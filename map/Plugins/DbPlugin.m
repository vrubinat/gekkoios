//
//  DbPlugin.m
//
//

#import "DbPlugin.h"
#import "UserData.h"

@implementation DbPlugin

id callbackId;


- (void)pluginInitialize
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                        selector:@selector(receiveTestNotification:)
                                                 name:@"CompliteChanges"
                                               object:nil];
   
    
    
    
}

- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"CompliteChanges"]){
        NSLog (@"Successfully received the test notification!");
        [self SyncComplete];
    }
    
}

- (void)sincro:(CDVInvokedUrlCommand*)command
{
    NSLog(@"loggin");
        

    callbackId = command.callbackId;
    CDVPluginResult* pluginResult = nil;
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    
}
- (void)getAll:(CDVInvokedUrlCommand*)command{
    
    callbackId = command.callbackId;
    CDVPluginResult* pluginResult = nil;
    
    
    UserData* db = [UserData sharedInstance];
    CBLDatabase * d = [db getDatabase];
    
    CBLQuery* query = d.queryAllDocuments;
    
    
    NSMutableArray *data = [[NSMutableArray alloc] init];
    
    for (CBLQueryRow* row in query.rows) {
        CBLDocument * doc = row.document;
        [data addObject:doc.properties];
    }
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:data];
    
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    

}
- (void)get:(CDVInvokedUrlCommand*)command{
    
    callbackId = command.callbackId;
    CDVPluginResult* pluginResult = nil;
    NSString* id = [command.arguments objectAtIndex:0];
    
    UserData* db = [UserData sharedInstance];
    CBLDatabase * d = [db getDatabase];
    
    CBLDocument* doc=[d documentWithID:id];
    
    if (doc){
      pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:doc.properties];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    
    
}
- (void)delete:(CDVInvokedUrlCommand*)command{
    
    //NSLog(@"delete data!!!!");
    callbackId = command.callbackId;
    CDVPluginResult* pluginResult = nil;
    NSString* id = [command.arguments objectAtIndex:0];
    

    UserData* db = [UserData sharedInstance];
    CBLDatabase * d = [db getDatabase];
    
    CBLDocument* doc=[d documentWithID:id];
    
    if (doc){
        NSError * error;
        if (![doc deleteDocument:&error]){
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:error.description];
        }else{
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"OK"];
        }

    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    
    
}
- (void)put:(CDVInvokedUrlCommand*)command{
    
    //NSLog(@"put data!!!!");
    callbackId = command.callbackId;
    CDVPluginResult* pluginResult = nil;
    NSMutableDictionary* data = [command.arguments objectAtIndex:0];
    
        UserData* db = [UserData sharedInstance];
    CBLDatabase * d = [db getDatabase];
    
    NSString *id = [data valueForKey:@"_id"];

    NSError * error;
    if (id){
        //update
        NSLog(@"Update");
        CBLDocument* doc=[d documentWithID:id];
        if (![doc putProperties:data error:&error]){
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:error.description];
            
        }
        
    }else{
        //insert
        NSLog(@"Insert");
        CBLDocument* doc = [d untitledDocument];
        if (![doc putProperties: data error: &error]){
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:error.description];
            
        }
    }
    
    if (!error){
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"OK"];
    }
    
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:callbackId];
    
    
}


- (void) SyncComplete {
    
    NSString* jsCallback = [NSString stringWithFormat:
                            @"window.updateData('%@');",
                            @"oooeoeoe"];
    [super writeJavascript:jsCallback];
    

}

@end
