//
//  ConfigSingleton.m
//  map
//
//  Created by victor rubinat on 29/05/13.
//
//


#import "UserData.h"


@interface UserData ()
@property(nonatomic, strong)CBLDatabase *database;
@property(nonatomic, strong)NSMutableDictionary *typeRevs;


@end



@implementation UserData

@synthesize database;
@synthesize typeRevs;




CBLReplication* _pull;
CBLReplication* _push;


BOOL backgroundMode;

static UserData *sharedInstance = nil;


// Get the shared instance and create it if necessary.
+ (UserData *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}

// We can still have a regular init method, that will get called the first time the Singleton is used.
- (id)init
{
    self = [super init];
    
    if (self) {
        // Work your initialising magic here as you normally would
        self.typeRevs = [NSMutableDictionary dictionary];
    }
    
    
    return self;
}

- (CBLDatabase * )getDatabase {

    return self.database;
};
- (void)useDatabase:(CBLDatabase*)theDatabase  {
    
    self.database = theDatabase;
    self.urlRemoteDB =    [NSURL URLWithString:@"https://aure.iriscouch.com:6984/gecko"];
   
    [self sincro];
}



- (void) sincro{

    NSLog(@"Start Config Replication");
    [self forgetSync];
    
    NSArray* repls = [self.database replicateWithURL: self.urlRemoteDB exclusively: YES];
    
    if (repls) {
        _pull = [repls objectAtIndex: 0];
        _push = [repls objectAtIndex: 1];
        
        _pull.continuous = YES;
        _push.continuous = YES;

        NSNotificationCenter* nctr = [NSNotificationCenter defaultCenter];
        [nctr addObserver: self selector: @selector(replicationProgress:)
                     name: kCBLReplicationChangeNotification object: _pull];
        [nctr addObserver: self selector: @selector(replicationProgress:)
                     name: kCBLReplicationChangeNotification object: _push];
        
        
    }
    
}

- (void) forgetSync {
    NSNotificationCenter* nctr = [NSNotificationCenter defaultCenter];
    if (_pull) {
        [nctr removeObserver: self name: nil object: _pull];
        _pull = nil;
    }
    if (_push) {
        [nctr removeObserver: self name: nil object: _push];
        _push = nil;
    }
}

- (void) replicationProgress: (NSNotificationCenter*)n {
    
    if (_pull.error){
        NSLog(@"Error: %@",_pull.error.description);
    }
    if (_push.error){
        NSLog(@"Error: %@",_push.error.description);
    }
    

    
    if (_pull.mode == kCBLReplicationActive || _push.mode == kCBLReplicationActive) {
        unsigned completed = _pull.completed + _push.completed;
        unsigned total = _pull.total + _push.total;
        NSLog(@"SYNC progress: %u / %u", completed, total);
     
    } else {
        NSLog(@"Complete XXXX");
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"CompliteChanges"
         object:self];
        

        //[self debug];

    }
}



- (void) debug{
     NSLog(@"Debug Data:");
    CBLQuery* query = self.database.queryAllDocuments;
    for (CBLQueryRow* row in query.rows) {
        CBLDocument * doc = row.document;
        NSLog(@"Doc ID = %@", row.key);
        NSLog(@" info :  %@ - %@ - %@",[doc.properties objectForKey: @"title"],[doc.properties objectForKey: @"auhtor"],[doc.properties objectForKey: @"image"]);
    }
    
}






@end
