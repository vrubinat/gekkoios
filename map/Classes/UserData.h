//
//  ConfigSingleton.h
//  map
//
//  Created by victor rubinat on 29/05/13.
//
//

#import <Foundation/Foundation.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import <CouchbaseLite/CBLJSON.h>



@interface UserData : NSObject{
    CBLDatabase *database;
    NSMutableDictionary *typeRevs;
}
    + (id)sharedInstance;
    - (void)useDatabase:(CBLDatabase*)theDatabase;
    - (void)sincro;
    - (void)debug;
    - (CBLDatabase * )getDatabase;



    @property (nonatomic, retain) NSURL* urlRemoteDB;




@end
