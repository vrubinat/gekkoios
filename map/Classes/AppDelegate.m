/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 */

//
//  AppDelegate.m
//  gg
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  Copyright ___ORGANIZATIONNAME___ ___YEAR___. All rights reserved.
//

#define DEBUG YES

#import "AppDelegate.h"
#import "MainViewController.h"

#import <Cordova/CDVPlugin.h>


#import "UserData.h"


@implementation AppDelegate

@synthesize window, viewController;

BOOL started;

- (id)init
{
    /** If you need to do any extra app-specific initialization, you can do it here
     *  -jm
     **/
    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];

    [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];

    int cacheSizeMemory = 8 * 1024 * 1024; // 8MB
    int cacheSizeDisk = 32 * 1024 * 1024; // 32MB
#if __has_feature(objc_arc)
        NSURLCache* sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
#else
        NSURLCache* sharedCache = [[[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"] autorelease];
#endif
    [NSURLCache setSharedURLCache:sharedCache];

    self = [super init];
    
    // create Databases
    [self createDatabase];
    
   
    
    return self;
}

#pragma mark UIApplicationDelegate implementation


- (void)applicationWillTerminate:(UIApplication *)application{
    NSLog(@"CCLOSSSEE");
}

/**
 * This is main kick off after the app inits, the views and Settings are setup here. (preferred - iOS4 and up)
 */
- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];

#if __has_feature(objc_arc)
        self.window = [[UIWindow alloc] initWithFrame:screenBounds];
#else
        self.window = [[[UIWindow alloc] initWithFrame:screenBounds] autorelease];
#endif
    self.window.autoresizesSubviews = YES;

#if __has_feature(objc_arc)
        self.viewController = [[MainViewController alloc] init];
#else
        self.viewController = [[[MainViewController alloc] init] autorelease];
#endif
    //self.viewController.useSplashScreen = YES;

    // Set your app's start page by setting the <content src='foo.html' /> tag in config.xml.
    // If necessary, uncomment the line below to override it.
  
    
    [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];

    started = NO;
    
    UserData* db = [UserData sharedInstance];
    
    [db useDatabase:self.DataDB];

    
    [self assignPage:db isStart:true];
    
    
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(changeAppStatus:)
     name:@"changeAppStatus"
     object:nil];
    
    // NOTE: To customize the view's frame size (which defaults to full screen), override
    // [self.viewController viewWillAppear:] in your view controller.

    self.window.rootViewController = self.viewController;
    
    
    [self.window makeKeyAndVisible];
    
       
    
    
    return YES;
}

-(void) changeAppStatus:(NSNotification *)notification{

    NSLog(@"Change AppStatus");
    

    
    UserData* db = [UserData sharedInstance];
    
    NSDictionary *dictionary = [notification userInfo];
    BOOL exit = NO;
    
    if (dictionary && [dictionary valueForKey:@"exit"]){
        exit=YES;
    }

    

    NSLog(@"start: %d exit:%d",started,exit);
    
    if (!started || exit ){
        [self assignPage:db isStart:false];
    }


}

-(void)assignPage:(UserData *)db isStart:(bool)start{
    
    NSString * page = @"index.html";

    if (! [self.viewController.startPage isEqualToString:page]){
        self.viewController.startPage = page;
        if (!start){
            NSString* startFilePath = [self.viewController.commandDelegate pathForResource:page];
            NSURL * appURL =[NSURL fileURLWithPath:startFilePath];
            NSLog(@"url: %@",[appURL absoluteString]);
            NSURLRequest* appReq = [NSURLRequest requestWithURL:appURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
            [self.viewController.webView loadRequest:appReq];


        }
     
    }

    
}



- (void)failed:(NSError*)error{
    
}

// this happens while we are running ( in the background, or from within our own app )
// only valid if gg-Info.plist specifies a protocol to handle
- (BOOL)application:(UIApplication*)application handleOpenURL:(NSURL*)url
{
    if (!url) {
        return NO;
    }

    // calls into javascript global function 'handleOpenURL'
    NSString* jsString = [NSString stringWithFormat:@"handleOpenURL(\"%@\");", url];
    [self.viewController.webView stringByEvaluatingJavaScriptFromString:jsString];

    // all plugins will get the notification, and their handlers will be called
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:CDVPluginHandleOpenURLNotification object:url]];

    return YES;
}

// repost the localnotification using the default NSNotificationCenter so multiple plugins may respond
- (void)            application:(UIApplication*)application
    didReceiveLocalNotification:(UILocalNotification*)notification
{
    // re-post ( broadcast )
    [[NSNotificationCenter defaultCenter] postNotificationName:CDVLocalNotification object:notification];
}

- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
{
    // iPhone doesn't support upside down by default, while the iPad does.  Override to allow all orientations always, and let the root view controller decide what's allowed (the supported orientations mask gets intersected).
    NSUInteger supportedInterfaceOrientations = (1 << UIInterfaceOrientationPortrait) | (1 << UIInterfaceOrientationLandscapeLeft) | (1 << UIInterfaceOrientationLandscapeRight) | (1 << UIInterfaceOrientationPortraitUpsideDown);

    return supportedInterfaceOrientations;
}



- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSLog(@"Start Backgroud");
    UIBackgroundTaskIdentifier bgTask=0;
    UIApplication *app = [UIApplication sharedApplication];
    bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    
    UserData* db = [UserData sharedInstance];
    [db sincro];
 

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    NSLog(@"app will enter foreground");
    
    NSString* jsString = [NSString stringWithFormat:@"test(\"%@\");", @"ss"];
    [self.viewController.webView  stringByEvaluatingJavaScriptFromString:jsString];
  
    
}
//PARA que esto funcione tengo que crear el provision Profile de la aplicacion
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken
{
    
  //  UserData* db = [UserData sharedInstance];
   // [db sincro];

}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{

}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //Aqui se reciven las notificaciones
    NSLog(@"alert msg - %@", [[userInfo objectForKey:@"aps"] objectForKey:@"alert"]);
    NSLog(@"alert custom - %@", [[userInfo objectForKey:@"customParam"] objectForKey:@"foo"]);
}
                           
- (void)applicationDidReceiveMemoryWarning:(UIApplication*)application
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

// Funciton to create Databases is need create Databases
// in App Delegate for use replication in background mode.
// If create databases in clases when app start in  background mode, the replication
// doesn't work.
-(void) createDatabase {
    

 
    NSError* error;
    
    self.DataDB = [[CBLManager sharedInstance] createDatabaseNamed: @"data" error: &error];
    
    if (!self.DataDB)
        NSLog(@"Error open Global Config Database: %@",error.localizedDescription);
    
   
    
}



@end
