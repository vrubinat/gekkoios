/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        console.log("AAAAAA");
        this.bindEvents();
        
        
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
   
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        var fs ;
        function dirReady(entry) {
            window.appRootDir = entry;
            console.log(JSON.stringify(window.appRootDir));
        }
        
        function cameraSuccess(imageData){
            
            function FileEntry(writer){
                console.log("WWWWW");
                console.log(imageData);
                writer.onwriteend = function(evt) {
                    console.log("FIN writter");
                };
                writer.write(imageData);
            }
            fs.root.getFile("img.png", {create: true, exclusive: false}, FileEntry, fail);
            console.log("asfda");
        }
        function cameraError(){
            
        }
        
        function onFileSystemSuccess(fileSystem) {
            fs =fileSystem;
            console.log("DIR: " + fileSystem.root.fullPath);
            fileSystem.root.getDirectory(".myapp", {
                                         create : true,
                                         exclusive : false
                                         }, dirReady, fail);
            
            navigator.camera.getPicture( cameraSuccess, cameraError );
        }
        
        function onResolveSuccess(fileEntry) {
            console.log(fileEntry.name);
        }
        
        function fail(evt) {
            console.log("F:" + evt.target.error.code);
        }

        window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem;
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFileSystemSuccess, fail);
        window.resolveLocalFileSystemURI("file:///example.txt", onResolveSuccess, fail);
        var conf = {
			"name" : "Basic",
			"init" : {
				"pos" : {
					"x" : 0,
					"y" : 0
				},
				"zoom" : 6
			},
			"bounds" : [
                        15673, 4943273, 380125, 5278373
                        ],
			"search" : {
				"views" : {
					"map" : {
						"class_name" : "m-red-square"
					}
				}
			}
		};
        
        var m = new Map('map', conf, function(api) {
                        console.log(api);
                        });
    },
    onFileSystemSuccess:function(fileSystem) {
        console.log("DIR:" + fileSystem.name);
    },
    
    onResolveSuccess:function(fileEntry) {
        console.log(fileEntry.name);
    },
    
    fail:function(evt) {
        console.log(evt.target.error.code);
    }
    
};
